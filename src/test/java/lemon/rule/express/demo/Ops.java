package lemon.rule.express.demo;

public class Ops {

    public boolean hasCompany(long id) {
        if (id % 2 == 1) {
            System.out.println("has company true");
            return true;
        } else {
            System.out.println("has company false");
            return false;
        }
    }

    public boolean saveInfo(BeanO bean, long buid) {
        System.out.println(bean.getName() + "_" + buid);
        return true;
    }

    public boolean breadflow(String cause) {
        System.out.println(cause);
        return true;
    }
}
