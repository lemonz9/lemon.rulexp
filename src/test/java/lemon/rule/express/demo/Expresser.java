package lemon.rule.express.demo;

import com.lemon.rule.express.DefaultContext;
import com.lemon.rule.express.ExpressRunner;
import com.lemon.rule.express.IExpressContext;

public class Expresser {

    private ExpressRunner runner = new ExpressRunner();

    public void init() {
        try {
            runner.addFunctionOfClassMethod("hascompany", Ops.class.getName(), "hasCompany", new String[] { "long" }, "company not exist");
            runner.addFunctionOfClassMethod("saveInfo", Ops.class.getName(), "saveInfo", new String[] { BeanO.class.getName(), "long" }, "save job error");
            runner.addFunctionOfClassMethod("breadflow", Ops.class.getName(), "breadflow", new String[] { "String" }, "break error");
            IExpressContext<String, Object> expressContext = new DefaultContext<String, Object>();
            expressContext.put("compid", new Long(12311));
            expressContext.put("buid", 12312);
            expressContext.put("beanobj", new BeanO(112, "插入这个职位"));

            String exp = "if(hascompany(compid)){saveInfo(beanobj,buid)}else{breadflow(\"保存职位出错,退出\")}";
            runner.execute(exp, expressContext, null, false, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Expresser demo = new Expresser();
        demo.init();
    }
}
