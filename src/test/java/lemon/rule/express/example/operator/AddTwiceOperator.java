package lemon.rule.express.example.operator;

import com.lemon.rule.express.Operator;

public class AddTwiceOperator extends Operator {

    public Object executeInner(Object[] list) throws Exception {
        int a = (Integer) list[0];
        int b = (Integer) list[1];
        return a + b + b;
    }

}
