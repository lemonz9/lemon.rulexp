package lemon.rule.express.example.operator;

import com.lemon.rule.express.Operator;

public class ApproveOperator extends Operator {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    int operater = 0;
    
    public ApproveOperator(int op) {
        this.operater = op;
    }

    @Override
    public Object executeInner(Object[] list) throws Exception {
        if (this.operater == 1) {
            System.out.println(list[0] + "审批:金额:" + list[1]);
            if (((Integer) list[1]) > 6000)
                return false;
        } else if (this.operater == 2)
            System.out.println("报销入卡:金额:" + list[0]);
        else
            System.out.println("重填:申请人:" + list[0]);
        return true;
    }

}
