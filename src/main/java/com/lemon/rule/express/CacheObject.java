package com.lemon.rule.express;

public class CacheObject {

    private String expressName;

    private String text;

    private InstructionSet instructionSet;

    public String getExpressName() {
        return expressName;
    }

    public void setExpressName(String name) {
        this.expressName = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public InstructionSet getInstructionSet() {
        return instructionSet;
    }

    public void setInstructionSet(InstructionSet instructionSet) {
        this.instructionSet = instructionSet;
    }

}
