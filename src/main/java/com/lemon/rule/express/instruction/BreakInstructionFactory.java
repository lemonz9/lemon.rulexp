package com.lemon.rule.express.instruction;

import java.util.Stack;

import com.lemon.rule.express.ExpressRunner;
import com.lemon.rule.express.InstructionSet;
import com.lemon.rule.express.instruction.detail.InstructionGoTo;
import com.lemon.rule.express.parse.ExpressNode;

public class BreakInstructionFactory extends InstructionFactory {
    public boolean createInstruction(ExpressRunner aCompile, InstructionSet result, Stack<ForRelBreakContinue> forStack, ExpressNode node, boolean isRoot) throws Exception {
        InstructionGoTo breakInstruction = new InstructionGoTo(result.getCurrentPoint() + 1);
        breakInstruction.name = "break";
        forStack.peek().breakList.add(breakInstruction);
        result.addInstruction(breakInstruction);
        return false;
    }
}
