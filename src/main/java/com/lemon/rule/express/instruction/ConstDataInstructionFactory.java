package com.lemon.rule.express.instruction;

import java.util.Stack;

import com.lemon.rule.express.ExpressRunner;
import com.lemon.rule.express.InstructionSet;
import com.lemon.rule.express.OperateData;
import com.lemon.rule.express.instruction.detail.InstructionConstData;
import com.lemon.rule.express.instruction.opdata.OperateClass;
import com.lemon.rule.express.parse.ExpressNode;

public class ConstDataInstructionFactory extends InstructionFactory {
    public OperateData genOperateData(ExpressNode node) {
        if (node.isTypeEqualsOrChild("CONST_CLASS")) {
            return new OperateClass(node.getValue(), (Class<?>) node.getObjectValue());
        } else {
            return new OperateData(node.getObjectValue(), node.getObjectValue().getClass());
        }
    }

    public boolean createInstruction(ExpressRunner aCompile, InstructionSet result, Stack<ForRelBreakContinue> forStack, ExpressNode node, boolean isRoot) throws Exception {
        result.addInstruction(new InstructionConstData(genOperateData(node)));
        return false;
    }
}
