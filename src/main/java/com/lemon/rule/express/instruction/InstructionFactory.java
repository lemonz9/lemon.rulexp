package com.lemon.rule.express.instruction;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import com.lemon.rule.express.ExpressRunner;
import com.lemon.rule.express.InstructionSet;
import com.lemon.rule.express.parse.ExpressNode;

public abstract class InstructionFactory {
    private static Map<String, InstructionFactory> instructionFactory = new HashMap<String, InstructionFactory>();

    public static InstructionFactory getInstructionFactory(String factory) {
        try {
            InstructionFactory result = instructionFactory.get(factory);
            if (result == null) {
                result = (InstructionFactory) Class.forName(factory).newInstance();
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public abstract boolean createInstruction(ExpressRunner aCompile, InstructionSet result, Stack<ForRelBreakContinue> forStack, ExpressNode node, boolean isRoot) throws Exception;
}
