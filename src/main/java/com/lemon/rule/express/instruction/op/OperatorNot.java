package com.lemon.rule.express.instruction.op;

import com.lemon.rule.express.Operator;

public class OperatorNot extends Operator {
    public OperatorNot(String name) {
        this.name = name;
    }

    public OperatorNot(String aAliasName, String aName, String aErrorInfo) {
        this.name = aName;
        this.aliasName = aAliasName;
        this.errorInfo = aErrorInfo;
    }

    public Object executeInner(Object[] list) throws Exception {
        return executeInner(list[0]);
    }

    public Object executeInner(Object op) throws Exception {
        Object result = null;
        if (op == null) {
            throw new Exception("null ����ִ�в�����" + this.getAliasName());
        }
        if (Boolean.class.equals(op.getClass()) == true) {
            boolean r = !((Boolean) op).booleanValue();
            result = Boolean.valueOf(r);
        } else {
            //
            String msg = "û�ж�������" + op.getClass().getName() + " �� " + this.name + "����";
            throw new Exception(msg);
        }
        return result;
    }
}