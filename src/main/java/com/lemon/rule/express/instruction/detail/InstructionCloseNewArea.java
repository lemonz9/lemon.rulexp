package com.lemon.rule.express.instruction.detail;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lemon.rule.express.InstructionSetContext;
import com.lemon.rule.express.RunEnvironment;

public class InstructionCloseNewArea extends Instruction {

    private static final Logger logger = LoggerFactory.getLogger(InstructionCloseNewArea.class);

    public void execute(RunEnvironment environment, List<String> errorList) throws Exception {
        //目前的模式，不需要执行任何操作
        if (environment.isTrace() && logger.isDebugEnabled()) {
            logger.debug(toString());
        }
        environment.setContext((InstructionSetContext) environment.getContext().getParent());
        environment.programPointAddOne();
    }

    public String toString() {
        return "closeNewArea";
    }
}