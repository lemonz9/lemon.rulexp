package com.lemon.rule.express.instruction;

import com.lemon.rule.express.CallResult;
import com.lemon.rule.express.ExpressLoader;
import com.lemon.rule.express.ExpressRunner;
import com.lemon.rule.express.IExpressContext;
import com.lemon.rule.express.InstructionSet;
import com.lemon.rule.express.InstructionSetContext;
import com.lemon.rule.express.OperateData;
import com.lemon.rule.express.RunEnvironment;
import com.lemon.rule.express.instruction.opdata.OperateDataArrayItem;
import com.lemon.rule.express.instruction.opdata.OperateDataAttr;
import com.lemon.rule.express.instruction.opdata.OperateDataField;
import com.lemon.rule.express.instruction.opdata.OperateDataKeyValue;
import com.lemon.rule.express.instruction.opdata.OperateDataLocalVar;

public interface IOperateDataCache {
    public OperateData fetchOperateData(Object obj, Class<?> aType);

    public OperateDataAttr fetchOperateDataAttr(String name, Class<?> aType);

    public OperateDataLocalVar fetchOperateDataLocalVar(String name, Class<?> aType);

    public OperateDataField fetchOperateDataField(Object aFieldObject, String aFieldName);

    public OperateDataArrayItem fetchOperateDataArrayItem(OperateData aArrayObject, int aIndex);

    public OperateDataKeyValue fetchOperateDataKeyValue(OperateData aKey, OperateData aValue);

    public RunEnvironment fetRunEnvironment(InstructionSet aInstructionSet, InstructionSetContext aContext, boolean aIsTrace);

    public CallResult fetchCallResult(Object aReturnValue, boolean aIsExit);

    public InstructionSetContext fetchInstructionSetContext(boolean aIsExpandToParent, ExpressRunner aRunner, IExpressContext<String, Object> aParent, ExpressLoader aExpressLoader, boolean aIsSupportDynamicFieldName);

    public void resetCache();

    public long getFetchCount();
}