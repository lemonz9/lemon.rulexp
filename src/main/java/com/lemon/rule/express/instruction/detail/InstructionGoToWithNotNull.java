package com.lemon.rule.express.instruction.detail;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lemon.rule.express.RunEnvironment;

public class InstructionGoToWithNotNull extends Instruction {
    private static final Logger logger = LoggerFactory.getLogger(InstructionCloseNewArea.class);
    int offset;
    boolean isPopStackData;

    public InstructionGoToWithNotNull(int aOffset, boolean aIsPopStackData) {
        this.offset = aOffset;
        this.isPopStackData = aIsPopStackData;
    }

    public void execute(RunEnvironment environment, List<String> errorList) throws Exception {
        Object o = null;
        if (this.isPopStackData == false) {
            o = environment.peek().getObject(environment.getContext());
        } else {
            o = environment.pop().getObject(environment.getContext());
        }
        if (o != null) {
            if (environment.isTrace() && logger.isDebugEnabled()) {
                logger.debug("goto +" + this.offset);
            }
            environment.gotoWithOffset(this.offset);
        } else {
            if (environment.isTrace() && logger.isDebugEnabled()) {
                logger.debug("programPoint ++ ");
            }
            environment.programPointAddOne();
        }
    }

    public String toString() {
        String result = "GoToIf[NOTNULL,isPop=" + this.isPopStackData + "] ";
        if (this.offset >= 0) {
            result = result + "+";
        }
        result = result + this.offset;
        return result;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public boolean isPopStackData() {
        return isPopStackData;
    }

    public void setPopStackData(boolean isPopStackData) {
        this.isPopStackData = isPopStackData;
    }

}