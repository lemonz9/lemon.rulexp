package com.lemon.rule.express.instruction.op;

import com.lemon.rule.express.InstructionSetContext;
import com.lemon.rule.express.OperateData;
import com.lemon.rule.express.instruction.OperateDataCacheManager;

public class OperatorField extends OperatorBase {

    public OperatorField() {
        this.name = "FieldCall";
    }

    public OperateData executeInner(InstructionSetContext parent, OperateData[] list) throws Exception {
        Object obj = list[0].getObject(parent);
        String fieldName = list[1].getObject(parent).toString();
        return OperateDataCacheManager.fetchOperateDataField(obj, fieldName);
    }
}