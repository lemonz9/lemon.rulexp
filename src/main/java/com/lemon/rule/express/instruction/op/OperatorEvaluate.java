package com.lemon.rule.express.instruction.op;

import com.lemon.rule.express.InstructionSetContext;
import com.lemon.rule.express.OperateData;
import com.lemon.rule.express.util.ExpressUtil;

public class OperatorEvaluate extends OperatorBase {
    public OperatorEvaluate(String name) {
        this.name = name;
    }

    public OperatorEvaluate(String aAliasName, String aName, String aErrorInfo) {
        this.name = aName;
        this.aliasName = aAliasName;
        this.errorInfo = aErrorInfo;
    }

    public OperateData executeInner(InstructionSetContext parent, OperateData[] list) throws Exception {
        return executeInner(parent, list[0], list[1]);
    }

    public OperateData executeInner(InstructionSetContext parent, OperateData op1, OperateData op2) throws Exception {
        Class<?> targetType = op1.getDefineType();
        Class<?> sourceType = op2.getType(parent);
        if (targetType != null) {
            if (ExpressUtil.isAssignable(targetType, sourceType) == false) {
                throw new Exception("��ֵʱ������ת������" + ExpressUtil.getClassName(sourceType) + " ����ת��Ϊ " + ExpressUtil.getClassName(targetType));
            }

        }
        Object result = op2.getObject(parent);
        if (targetType != null) {
            result = ExpressUtil.castObject(result, targetType, false);
        }
        op1.setObject(parent, result);
        return op1;
    }

}