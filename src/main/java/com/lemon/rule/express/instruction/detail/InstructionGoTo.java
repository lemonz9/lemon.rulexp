package com.lemon.rule.express.instruction.detail;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lemon.rule.express.RunEnvironment;

public class InstructionGoTo extends Instruction {
    private static final Logger logger = LoggerFactory.getLogger(InstructionCloseNewArea.class);
    /**
     * 跳转指令的偏移量
     */
    int offset;
    public String name;

    public InstructionGoTo(int aOffset) {
        this.offset = aOffset;
    }

    public void execute(RunEnvironment environment, List<String> errorList) throws Exception {
        if (environment.isTrace() && logger.isDebugEnabled()) {
            logger.debug(this.toString());
        }
        environment.gotoWithOffset(this.offset);
    }

    public String toString() {
        String result = (this.name == null ? "" : this.name + ":") + "GoTo ";
        if (this.offset >= 0) {
            result = result + "+";
        }
        result = result + this.offset + " ";
        return result;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

}
