package com.lemon.rule.express.instruction.op;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.lemon.rule.express.Operator;

public class OperatorPrintln extends Operator {
    private static final Logger logger = LoggerFactory.getLogger(OperatorPrintln.class);

    public OperatorPrintln(String name) {
        this.name = name;
    }

    public OperatorPrintln(String aAliasName, String aName, String aErrorInfo) {
        this.name = aName;
        this.aliasName = aAliasName;
        this.errorInfo = aErrorInfo;
    }

    public Object executeInner(Object[] list) throws Exception {
        if (list.length != 1) {
            throw new Exception("操作数异常,有且只能有一个操作数");
        }
        logger.info(JSON.toJSONString(list[0]));
        return null;
    }

}