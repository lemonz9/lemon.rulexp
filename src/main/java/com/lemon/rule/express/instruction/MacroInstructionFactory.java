package com.lemon.rule.express.instruction;

import java.util.Stack;

import com.lemon.rule.express.ExpressRunner;
import com.lemon.rule.express.InstructionSet;
import com.lemon.rule.express.parse.ExpressNode;

public class MacroInstructionFactory extends InstructionFactory {
    public boolean createInstruction(ExpressRunner aCompile, InstructionSet result, Stack<ForRelBreakContinue> forStack, ExpressNode node, boolean isRoot) throws Exception {
        ExpressNode[] children = node.getChildren();
        String macroName = children[0].getValue();
        ExpressNode macroRoot = new ExpressNode(aCompile.getNodeTypeManager().findNodeType("FUNCTION_DEFINE"), "macro-" + macroName);
        for (ExpressNode tempNode : children[1].getChildren()) {
            macroRoot.addLeftChild(tempNode);
        }
        InstructionSet macroInstructionSet = aCompile.createInstructionSet(macroRoot, InstructionSet.TYPE_MARCO);
        result.addMacroDefine(macroName, new FunctionInstructionSet(macroName, "macro", macroInstructionSet));
        return false;
    }
}
