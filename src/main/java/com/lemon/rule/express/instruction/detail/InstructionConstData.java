package com.lemon.rule.express.instruction.detail;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lemon.rule.express.OperateData;
import com.lemon.rule.express.RunEnvironment;
import com.lemon.rule.express.instruction.opdata.OperateDataAttr;

public class InstructionConstData extends Instruction {
    private static final Logger logger = LoggerFactory.getLogger(InstructionCloseNewArea.class);
    OperateData operateData;

    public InstructionConstData(OperateData data) {
        this.operateData = data;
    }

    public OperateData getOperateData() {
        return this.operateData;
    }

    public void execute(RunEnvironment environment, List<String> errorList) throws Exception {
        if (environment.isTrace() && logger.isDebugEnabled()) {
            if (this.operateData instanceof OperateDataAttr) {
                logger.debug(this + ":" + this.operateData.getObject(environment.getContext()));
            } else {
                logger.debug(this.toString());
            }
        }
        environment.push(this.operateData);
        environment.programPointAddOne();
    }

    public String toString() {
        if (this.operateData instanceof OperateDataAttr) {
            return "LoadData attr:" + this.operateData.toString();
        } else {
            return "LoadData " + this.operateData.toString();
        }
    }

}