package com.lemon.rule.express.instruction;

import java.util.Stack;

import com.lemon.rule.express.ExpressRunner;
import com.lemon.rule.express.InstructionSet;
import com.lemon.rule.express.instruction.detail.InstructionOperator;
import com.lemon.rule.express.instruction.op.OperatorBase;
import com.lemon.rule.express.parse.ExpressNode;
import com.lemon.rule.express.util.ExpressUtil;

public class NewInstructionFactory extends InstructionFactory {
    public boolean createInstruction(ExpressRunner aCompile, InstructionSet result, Stack<ForRelBreakContinue> forStack, ExpressNode node, boolean isRoot) throws Exception {
        OperatorBase op = aCompile.getOperatorFactory().newInstance("new");
        ExpressNode[] children = node.getChildren();
        if (node.isTypeEqualsOrChild("NEW_ARRAY")) {
            String tempStr = children[0].getValue();
            for (int i = 0; i < children.length - 1; i++) {
                tempStr = tempStr + "[]";
            }
            children[0].setValue(tempStr);
            children[0].setOrgiValue(tempStr);
            children[0].setObjectValue(ExpressUtil.getJavaClass(tempStr));
        } else if (node.isTypeEqualsOrChild("anonymousNewArray")) {
            op = aCompile.getOperatorFactory().newInstance("anonymousNewArray");
        }

        boolean returnVal = false;
        children = node.getChildren();// ��Ҫ���»�ȡ����
        for (int i = 0; i < children.length; i++) {
            boolean tmpHas = aCompile.createInstructionSetPrivate(result, forStack, children[i], false);
            returnVal = returnVal || tmpHas;
        }
        result.addInstruction(new InstructionOperator(op, children.length));
        return returnVal;
    }
}
