package com.lemon.rule.express.instruction.op;

import com.lemon.rule.express.InstructionSetContext;
import com.lemon.rule.express.OperateData;
import com.lemon.rule.express.instruction.OperateDataCacheManager;
import com.lemon.rule.express.instruction.opdata.OperateDataLocalVar;
import com.lemon.rule.express.instruction.opdata.OperateDataVirClass;

public class OperatorDef extends OperatorBase {
    public OperatorDef(String aName) {
        this.name = aName;
    }

    public OperatorDef(String aAliasName, String aName, String aErrorInfo) {
        this.name = aName;
        this.aliasName = aAliasName;
        this.errorInfo = aErrorInfo;
    }

    public OperateData executeInner(InstructionSetContext context, OperateData[] list) throws Exception {
        Object type = list[0].getObject(context);
        String varName = (String) list[1].getObject(context);
        Class<?> tmpClass = null;
        if (type instanceof Class) {
            tmpClass = (Class<?>) type;
        } else {
            tmpClass = OperateDataVirClass.class;
        }
        OperateDataLocalVar result = OperateDataCacheManager.fetchOperateDataLocalVar(varName, tmpClass);
        context.addSymbol(varName, result);
        return result;
    }
}