package com.lemon.rule.express.instruction.op;

import com.lemon.rule.express.IExpressContext;
import com.lemon.rule.express.InstructionSetContext;
import com.lemon.rule.express.OperateData;

/**
 * ���� ",","(",")",";"
 */

public class OperatorNullOp extends OperatorBase {
    public OperatorNullOp(String name) {
        this.name = name;
    }

    public OperatorNullOp(String aAliasName, String aName, String aErrorInfo) {
        this.name = aName;
        this.aliasName = aAliasName;
        this.errorInfo = aErrorInfo;
    }

    public OperateData executeInner(InstructionSetContext parent, OperateData[] list) throws Exception {
        return executeInner(parent);
    }

    public OperateData executeInner(IExpressContext<String, Object> parent) throws Exception {
        return null;
    }
}