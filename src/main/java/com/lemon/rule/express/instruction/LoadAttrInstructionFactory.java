package com.lemon.rule.express.instruction;

import java.util.Stack;

import com.lemon.rule.express.ExpressRunner;
import com.lemon.rule.express.InstructionSet;
import com.lemon.rule.express.instruction.detail.InstructionCallMacro;
import com.lemon.rule.express.instruction.detail.InstructionLoadAttr;
import com.lemon.rule.express.parse.ExpressNode;

public class LoadAttrInstructionFactory extends InstructionFactory {
    public boolean createInstruction(ExpressRunner aCompile, InstructionSet result, Stack<ForRelBreakContinue> forStack, ExpressNode node, boolean isRoot) throws Exception {
        FunctionInstructionSet functionSet = result.getMacroDefine(node.getValue());
        if (functionSet != null) {//�Ǻ궨��
            result.insertInstruction(result.getCurrentPoint() + 1, new InstructionCallMacro(node.getValue()));
        } else {
            result.addInstruction(new InstructionLoadAttr(node.getValue()));
            if (node.getChildren().length > 0) {
                throw new Exception("���ʽ���ô���");
            }
        }
        return false;
    }
}
