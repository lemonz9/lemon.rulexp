package com.lemon.rule.express.instruction.op;

import java.util.ArrayList;
import java.util.List;

import com.lemon.rule.express.InstructionSetContext;
import com.lemon.rule.express.OperateData;
import com.lemon.rule.express.instruction.OperateDataCacheManager;

public class OperatorAnonymousNewList extends OperatorBase {
    public OperatorAnonymousNewList(String aName) {
        this.name = aName;
    }

    public OperatorAnonymousNewList(String aAliasName, String aName, String aErrorInfo) {
        this.name = aName;
        this.aliasName = aAliasName;
        this.errorInfo = aErrorInfo;
    }

    public OperateData executeInner(InstructionSetContext context, OperateData[] list) throws Exception {
        List<Object> result = new ArrayList<Object>();
        for (int i = 0; i < list.length; i++) {
            result.add(list[i].getObject(context));
        }
        return OperateDataCacheManager.fetchOperateData(result, List.class);
    }
}
