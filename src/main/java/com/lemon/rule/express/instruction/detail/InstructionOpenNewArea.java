package com.lemon.rule.express.instruction.detail;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lemon.rule.express.InstructionSetContext;
import com.lemon.rule.express.RunEnvironment;
import com.lemon.rule.express.instruction.OperateDataCacheManager;

public class InstructionOpenNewArea extends Instruction {

    private static final Logger logger = LoggerFactory.getLogger(InstructionCloseNewArea.class);

    public void execute(RunEnvironment environment, List<String> errorList) throws Exception {
        if (environment.isTrace() && logger.isDebugEnabled()) {
            logger.debug(this.toString());
        }
        InstructionSetContext parentContext = environment.getContext();
        environment.setContext(OperateDataCacheManager.fetchInstructionSetContext(true, parentContext.getExpressRunner(), parentContext, parentContext.getExpressLoader(), parentContext.isSupportDynamicFieldName()));
        environment.programPointAddOne();
    }

    public String toString() {
        return "openNewArea";
    }
}
