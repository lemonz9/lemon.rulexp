package com.lemon.rule.express.instruction.detail;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lemon.rule.express.RunEnvironment;

public class InstructionReturn extends Instruction {
    private static final Logger logger = LoggerFactory.getLogger(InstructionReturn.class);

    boolean haveReturnValue;

    public InstructionReturn(boolean aHaveReturnValue) {
        this.haveReturnValue = aHaveReturnValue;
    }

    public void execute(RunEnvironment environment, List<String> errorList) throws Exception {
        if (environment.isTrace() && logger.isDebugEnabled()) {
            logger.debug(toString());
        }
        if (this.haveReturnValue == true) {
            environment.quitExpress(environment.pop().getObject(environment.getContext()));
        } else {
            environment.quitExpress();
        }
        environment.programPointAddOne();
    }

    public String toString() {
        if (this.haveReturnValue) {
            return "return [value]";
        } else {
            return "return";
        }
    }
}