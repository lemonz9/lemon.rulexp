package com.lemon.rule.express.instruction.op;

import com.lemon.rule.express.InstructionSetContext;
import com.lemon.rule.express.OperateData;
import com.lemon.rule.express.OperatorOfNumber;
import com.lemon.rule.express.instruction.OperateDataCacheManager;
import com.lemon.rule.express.util.ExpressUtil;

public class OperatorDoubleAddReduce extends OperatorBase {
    public OperatorDoubleAddReduce(String name) {
        this.name = name;
    }

    public OperateData executeInner(InstructionSetContext parent, OperateData[] list) throws Exception {
        Object obj = list[0].getObject(parent);
        Object result = null;
        if (this.getName().equals("++")) {
            result = OperatorOfNumber.add(obj, 1, this.isPrecise);
        } else if (this.getName().equals("--")) {
            result = OperatorOfNumber.subtract(obj, 1, this.isPrecise);
        }
        ((OperateData) list[0]).setObject(parent, result);

        if (result == null) {
            return OperateDataCacheManager.fetchOperateData(null, null);
        } else {
            return OperateDataCacheManager.fetchOperateData(result, ExpressUtil.getSimpleDataType(result.getClass()));
        }
    }
}