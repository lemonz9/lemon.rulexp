package com.lemon.rule.express.instruction;

import java.util.Stack;

import com.lemon.rule.express.ExpressRunner;
import com.lemon.rule.express.InstructionSet;
import com.lemon.rule.express.instruction.detail.InstructionClearDataStack;
import com.lemon.rule.express.instruction.detail.InstructionCloseNewArea;
import com.lemon.rule.express.instruction.detail.InstructionOpenNewArea;
import com.lemon.rule.express.parse.ExpressNode;

public class BlockInstructionFactory extends InstructionFactory {
    public boolean createInstruction(ExpressRunner aCompile, InstructionSet result, Stack<ForRelBreakContinue> forStack, ExpressNode node, boolean isRoot) throws Exception {
        if (node.isTypeEqualsOrChild("STAT_SEMICOLON") && result.getCurrentPoint() >= 0 && result.getInstruction(result.getCurrentPoint()) instanceof InstructionClearDataStack == false) {
            result.addInstruction(new InstructionClearDataStack());
        }

        int tmpPoint = result.getCurrentPoint() + 1;
        boolean returnVal = false;
        boolean hasDef = false;
        for (ExpressNode tmpNode : node.getChildren()) {
            boolean tmpHas = aCompile.createInstructionSetPrivate(result, forStack, tmpNode, false);
            hasDef = hasDef || tmpHas;
        }
        if (hasDef == true && isRoot == false && node.getTreeType().isEqualsOrChild("STAT_BLOCK")) {
            result.insertInstruction(tmpPoint, new InstructionOpenNewArea());
            result.insertInstruction(result.getCurrentPoint() + 1, new InstructionCloseNewArea());
            returnVal = false;
        } else {
            returnVal = hasDef;
        }
        return returnVal;
    }
}
