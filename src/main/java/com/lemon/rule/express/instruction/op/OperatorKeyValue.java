package com.lemon.rule.express.instruction.op;

import com.lemon.rule.express.InstructionSetContext;
import com.lemon.rule.express.OperateData;
import com.lemon.rule.express.instruction.OperateDataCacheManager;

public class OperatorKeyValue extends OperatorBase {

    public OperatorKeyValue(String aName) {
        this.name = aName;
    }

    public OperatorKeyValue(String aAliasName, String aName, String aErrorInfo) {
        this.name = aName;
        this.aliasName = aAliasName;
        this.errorInfo = aErrorInfo;
    }

    public OperateData executeInner(InstructionSetContext context, OperateData[] list) throws Exception {
        return OperateDataCacheManager.fetchOperateDataKeyValue(list[0], list[1]);
    }
}
