package com.lemon.rule.express.instruction.detail;

import java.util.List;

import com.lemon.rule.express.RunEnvironment;

/**
 * 指令
 *
 * @author WangYazhou
 * @date  2016年7月15日 下午7:05:12
 * @see
 */
public abstract class Instruction {

    public abstract void execute(RunEnvironment environment, List<String> errorList) throws Exception;
}
