package com.lemon.rule.express.instruction.op;

import com.lemon.rule.express.InstructionSetContext;
import com.lemon.rule.express.OperateData;
import com.lemon.rule.express.instruction.OperateDataCacheManager;
import com.lemon.rule.express.util.ExpressUtil;

public class OperatorCast extends OperatorBase {
    public OperatorCast(String aName) {
        this.name = aName;
    }

    public OperateData executeInner(InstructionSetContext parent, OperateData[] list) throws Exception {
        Class<?> tmpClass = (Class<?>) list[0].getObject(parent);
        Object castObj = ExpressUtil.castObject(list[1].getObject(parent), tmpClass, true);
        OperateData result = OperateDataCacheManager.fetchOperateData(castObj, tmpClass);
        return result;
    }
}