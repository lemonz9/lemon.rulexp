package com.lemon.rule.express.instruction.detail;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lemon.rule.express.InstructionSet;
import com.lemon.rule.express.RunEnvironment;
import com.lemon.rule.express.instruction.opdata.OperateDataAttr;

public class InstructionLoadAttr extends Instruction {
    private static final Logger logger = LoggerFactory.getLogger(InstructionCloseNewArea.class);
    String attrName;

    public InstructionLoadAttr(String aName) {
        this.attrName = aName;
    }

    public String getAttrName() {
        return this.attrName;
    }

    public void execute(RunEnvironment environment, List<String> errorList) throws Exception {
        Object o = environment.getContext().getSymbol(this.attrName);
        if (o != null && o instanceof InstructionSet) {//是函数，则执行
            if (environment.isTrace() && logger.isDebugEnabled()) {
                logger.info("command transfer： LoadAttr -- >CallMacro ");
            }
            InstructionCallMacro macro = new InstructionCallMacro(this.attrName);
            //            macro.setLog(this.logg);
            macro.execute(environment, errorList);
            //注意，此处不能在增加指令，因为在InstructionCallMacro已经调用 environment.programPointAddOne();
        } else {
            if (environment.isTrace() && logger.isDebugEnabled()) {
                logger.debug(this.toString() + ":" + ((OperateDataAttr) o).getObject(environment.getContext()));
            }
            environment.push((OperateDataAttr) o);
            environment.programPointAddOne();
        }
    }

    public String toString() {
        return "LoadAttr:" + this.attrName;
    }
}
