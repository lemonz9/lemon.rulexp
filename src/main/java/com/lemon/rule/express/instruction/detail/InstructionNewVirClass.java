package com.lemon.rule.express.instruction.detail;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lemon.rule.express.OperateData;
import com.lemon.rule.express.RunEnvironment;
import com.lemon.rule.express.instruction.opdata.OperateDataAttr;
import com.lemon.rule.express.instruction.opdata.OperateDataVirClass;

public class InstructionNewVirClass extends Instruction {
    private static final Logger logger = LoggerFactory.getLogger(InstructionCloseNewArea.class);
    String className;
    int opDataNumber;

    public InstructionNewVirClass(String name, int aOpDataNumber) {
        this.className = name;
        this.opDataNumber = aOpDataNumber;
    }

    public void execute(RunEnvironment environment, List<String> errorList) throws Exception {
        OperateData[] parameters = environment.popArray(environment.getContext(), this.opDataNumber);
        if (environment.isTrace() && logger.isDebugEnabled()) {
            String str = "new VClass(";
            for (int i = 0; i < parameters.length; i++) {
                if (i > 0) {
                    str = str + ",";
                }
                if (parameters[i] instanceof OperateDataAttr) {
                    str = str + parameters[i] + ":" + parameters[i].getObject(environment.getContext());
                } else {
                    str = str + parameters[i];
                }
            }
            str = str + ")";
            logger.debug(str);
        }
        OperateDataVirClass result = new OperateDataVirClass(className);
        environment.push(result);
        environment.programPointAddOne();
        result.initialInstance(environment.getContext(), parameters, errorList, environment.isTrace());
    }

    public String toString() {
        return "new VClass[" + this.className + "] OPNUMBER[" + this.opDataNumber + "]";
    }

}