package com.lemon.rule.express.instruction;

import java.util.Stack;

import com.lemon.rule.express.ExpressRunner;
import com.lemon.rule.express.InstructionSet;
import com.lemon.rule.express.instruction.detail.InstructionGoTo;
import com.lemon.rule.express.parse.ExpressNode;

public class ContinueInstructionFactory extends InstructionFactory {
    public boolean createInstruction(ExpressRunner aCompile, InstructionSet result, Stack<ForRelBreakContinue> forStack, ExpressNode node, boolean isRoot) throws Exception {
        InstructionGoTo continueInstruction = new InstructionGoTo(result.getCurrentPoint() + 1);
        continueInstruction.name = "continue";
        forStack.peek().continueList.add(continueInstruction);
        result.addInstruction(continueInstruction);
        return false;
    }
}
