package com.lemon.rule.express;

import java.util.concurrent.ConcurrentMap;

import com.google.common.collect.Maps;

/**
 * 
 * 
 * @author WangYazhou
 * @date  2016年7月15日 上午11:14:43
 * @see
 */
public class LocalExpressCacheRunner extends ExpressRemoteCacheRunner {

    private static ConcurrentMap<String, Object> expressMap = Maps.newConcurrentMap();
    
    private ExpressRunner expressRunner;

    public LocalExpressCacheRunner(ExpressRunner expressRunner) {
        this.expressRunner = expressRunner;
    }

    @Override
    public final Object getCache(String key) {
        return expressMap.get(key);
    }

    @Override
    public final void putCache(String key, Object object) {
        expressMap.put(key, object);
    }

    @Override
    public final ExpressRunner getExpressRunner() {
        return this.expressRunner;
    }

}
