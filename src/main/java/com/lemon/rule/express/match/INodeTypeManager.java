package com.lemon.rule.express.match;

public interface INodeTypeManager {
    public INodeType findNodeType(String name);
}
